package main

import (
	"flag"
	"log"
	"github.com/go-openapi/loads"
	"royceApi/api/restapi"
	"royceApi/api/restapi/operations"
	_ "github.com/sakirsensoy/genv/dotenv/autoload"
)

var portFlag = flag.Int("port", 3000, "Port to run this service on")

func main()  {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewUserAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer server.Shutdown()
	flag.Parse()
	server.Port = *portFlag
	server.ConfigureAPI()

	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}