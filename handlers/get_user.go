package handlers

import (
	"royceApi/api/restapi/operations/user"
	"github.com/go-openapi/runtime/middleware"
	"gorm.io/gorm"
	"royceApi/internal/repositories"
)

type getUser struct {
	db *gorm.DB
	userRepository repositories.UserHandler
}

func NewGetUserByIdHandler(db *gorm.DB, repo repositories.UserHandler) user.GetUserByIDHandler {
	return &getUser{
		db: db,
		userRepository: repo,
	}
}

func (a *getUser) Handle(params user.GetUserByIDParams) middleware.Responder {
	model, err := a.userRepository.FetchUserById(a.db, params.ID)
	if err != nil {
		// handle error by logger ex: a.Logger.Notify()
		return user.NewGetUserByIDNotFound()
	}

	return user.NewGetUserByIDOK().WithPayload(model)
}
