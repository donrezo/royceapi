package handlers

import (
	"royceApi/api/restapi/operations/user"
	"github.com/go-openapi/runtime/middleware"
	"gorm.io/gorm"
	"royceApi/internal/repositories"
)

type editUser struct {
	db *gorm.DB
	userRepository repositories.UserHandler
}

func NewEditUserHandler(db *gorm.DB, repo repositories.UserHandler) user.EditUserHandler {
	return &editUser{
		db: db,
		userRepository: repo,
	}
}

func (a *editUser) Handle(params user.EditUserParams) middleware.Responder {
	model, err := a.userRepository.EditUserById(a.db,params.ID, params)
	if err != nil {
		// handle error by logger ex: a.Logger.Notify()
		return user.NewEditUserMethodNotAllowed()
	}
	return user.NewEditUserOK().WithPayload(model)
}
