package handlers

import (
	"royceApi/api/restapi/operations/user"
	"github.com/go-openapi/runtime/middleware"
	"gorm.io/gorm"
	"royceApi/internal/repositories"
)

type addUser struct {
	db *gorm.DB
	userRepository repositories.UserHandler
}

func NewAddUserHandler(db *gorm.DB, repo repositories.UserHandler) user.AddUserHandler {
	return &addUser{
		db: db,
		userRepository: repo,
	}
}

func (a *addUser) Handle(params user.AddUserParams) middleware.Responder {

	model, err := a.userRepository.CreateNewUser(a.db, params)
	if err != nil {
		// handle error by logger ex: a.Logger.Notify()
		return user.NewAddUserMethodNotAllowed()
	}
	return user.NewAddUserOK().WithPayload(model)
}
