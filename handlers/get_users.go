package handlers

import (
	"royceApi/api/restapi/operations/user"
	"github.com/go-openapi/runtime/middleware"
	"gorm.io/gorm"
	"royceApi/internal/repositories"
)

type getUsers struct {
	db *gorm.DB
	userRepository repositories.UserHandler
}

func NewGetUsersHandler(db *gorm.DB, repo repositories.UserHandler) user.GetUsersHandler {
	return &getUsers{
		db: db,
		userRepository: repo,
	}
}

func (a *getUsers) Handle(params user.GetUsersParams) middleware.Responder {
	models, err := a.userRepository.FetchAllUsers(a.db)
	if err != nil {
		// handle error by logger ex: a.Logger.Notify()
		user.NewGetUsersUnauthorized()
	}
	return user.NewGetUsersOK().WithPayload(models)
}
