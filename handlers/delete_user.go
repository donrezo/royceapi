package handlers

import (
	"royceApi/api/restapi/operations/user"
	"github.com/go-openapi/runtime/middleware"
	"gorm.io/gorm"
	"royceApi/internal/repositories"
)

type deleteUser struct {
	db *gorm.DB
	userRepository repositories.UserHandler
}

func NewDeleteUserHandler(db *gorm.DB, repo repositories.UserHandler) user.DeleteUserByIDHandler {
	return &deleteUser{
		db: db,
		userRepository: repo,
	}
}

func (a *deleteUser) Handle(params user.DeleteUserByIDParams) middleware.Responder {
	_, err := a.userRepository.DeleteUserById(a.db, params.ID)
	if err != nil {
		// handle error by logger ex: a.Logger.Notify()
		return user.NewDeleteUserByIDNotFound()
	}
	return user.NewDeleteUserByIDOK()
}
