// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// NewEditUserParams creates a new EditUserParams object
// no default values defined in spec.
func NewEditUserParams() EditUserParams {

	return EditUserParams{}
}

// EditUserParams contains all the bound params for the edit user operation
// typically these are obtained from a http.Request
//
// swagger:parameters editUser
type EditUserParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*This text will be saved as a address
	  In: formData
	*/
	Address *string
	/*This text will be saved as a description
	  In: formData
	*/
	Description *string
	/*This date will be saved as date of brith
	  In: formData
	*/
	Dob *int32
	/*The id will be used as identificator of user to edit
	  Required: true
	  In: formData
	*/
	ID string
	/*This text will be saved as a name
	  In: formData
	*/
	Name *string
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewEditUserParams() beforehand.
func (o *EditUserParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	if err := r.ParseMultipartForm(32 << 20); err != nil {
		if err != http.ErrNotMultipart {
			return errors.New(400, "%v", err)
		} else if err := r.ParseForm(); err != nil {
			return errors.New(400, "%v", err)
		}
	}
	fds := runtime.Values(r.Form)

	fdAddress, fdhkAddress, _ := fds.GetOK("address")
	if err := o.bindAddress(fdAddress, fdhkAddress, route.Formats); err != nil {
		res = append(res, err)
	}

	fdDescription, fdhkDescription, _ := fds.GetOK("description")
	if err := o.bindDescription(fdDescription, fdhkDescription, route.Formats); err != nil {
		res = append(res, err)
	}

	fdDob, fdhkDob, _ := fds.GetOK("dob")
	if err := o.bindDob(fdDob, fdhkDob, route.Formats); err != nil {
		res = append(res, err)
	}

	fdID, fdhkID, _ := fds.GetOK("id")
	if err := o.bindID(fdID, fdhkID, route.Formats); err != nil {
		res = append(res, err)
	}

	fdName, fdhkName, _ := fds.GetOK("name")
	if err := o.bindName(fdName, fdhkName, route.Formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// bindAddress binds and validates parameter Address from formData.
func (o *EditUserParams) bindAddress(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: false

	if raw == "" { // empty values pass all other validations
		return nil
	}

	o.Address = &raw

	return nil
}

// bindDescription binds and validates parameter Description from formData.
func (o *EditUserParams) bindDescription(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: false

	if raw == "" { // empty values pass all other validations
		return nil
	}

	o.Description = &raw

	return nil
}

// bindDob binds and validates parameter Dob from formData.
func (o *EditUserParams) bindDob(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: false

	if raw == "" { // empty values pass all other validations
		return nil
	}

	value, err := swag.ConvertInt32(raw)
	if err != nil {
		return errors.InvalidType("dob", "formData", "int32", raw)
	}
	o.Dob = &value

	return nil
}

// bindID binds and validates parameter ID from formData.
func (o *EditUserParams) bindID(rawData []string, hasKey bool, formats strfmt.Registry) error {
	if !hasKey {
		return errors.Required("id", "formData", rawData)
	}
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: true

	if err := validate.RequiredString("id", "formData", raw); err != nil {
		return err
	}

	o.ID = raw

	return nil
}

// bindName binds and validates parameter Name from formData.
func (o *EditUserParams) bindName(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: false

	if raw == "" { // empty values pass all other validations
		return nil
	}

	o.Name = &raw

	return nil
}
