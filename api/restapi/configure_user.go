// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"
	_ "github.com/sakirsensoy/genv/dotenv/autoload"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"royceApi/api/restapi/operations"
	"royceApi/api/restapi/operations/user"
	"gorm.io/gorm"
	"royceApi/configs"
	"gorm.io/driver/mysql"
	"royceApi/internal/repositories"
	"royceApi/handlers"
	"royceApi/internal/entities"
)

//go:generate swagger generate server --target ../../api --name User --spec ../swagger/swagger.yml --principal interface{} --exclude-main

func configureFlags(api *operations.UserAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.UserAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	db, err := gorm.Open(mysql.Open(configs.App.DSN),  &gorm.Config{})
	if err != nil {
		//Handle error
		return nil
	}
	err = db.Migrator().AutoMigrate(&entities.User{})
	if err != nil {
		//Handle error
		return nil
	}

	repo := repositories.ProvideUserRepository()

	api.UseSwaggerUI()

	api.UserAddUserHandler = handlers.NewAddUserHandler(db, repo)
	api.UserDeleteUserByIDHandler = handlers.NewDeleteUserHandler(db, repo)
	api.UserEditUserHandler = handlers.NewEditUserHandler(db, repo)
	api.UserGetUserByIDHandler = handlers.NewGetUserByIdHandler(db, repo)
	api.UserGetUsersHandler = handlers.NewGetUsersHandler(db, repo)

	api.UrlformConsumer = runtime.DiscardConsumer

	api.JSONProducer = runtime.JSONProducer()
	api.XMLProducer = runtime.XMLProducer()

	if api.UserAddUserHandler == nil {
		api.UserAddUserHandler = user.AddUserHandlerFunc(func(params user.AddUserParams) middleware.Responder {
			return middleware.NotImplemented("operation user.AddUser has not yet been implemented")
		})
	}
	if api.UserDeleteUserByIDHandler == nil {
		api.UserDeleteUserByIDHandler = user.DeleteUserByIDHandlerFunc(func(params user.DeleteUserByIDParams) middleware.Responder {
			return middleware.NotImplemented("operation user.DeleteUserByID has not yet been implemented")
		})
	}
	if api.UserEditUserHandler == nil {
		api.UserEditUserHandler = user.EditUserHandlerFunc(func(params user.EditUserParams) middleware.Responder {
			return middleware.NotImplemented("operation user.EditUser has not yet been implemented")
		})
	}
	if api.UserGetUserByIDHandler == nil {
		api.UserGetUserByIDHandler = user.GetUserByIDHandlerFunc(func(params user.GetUserByIDParams) middleware.Responder {
			return middleware.NotImplemented("operation user.GetUserByID has not yet been implemented")
		})
	}
	if api.UserGetUsersHandler == nil {
		api.UserGetUsersHandler = user.GetUsersHandlerFunc(func(params user.GetUsersParams) middleware.Responder {
			return middleware.NotImplemented("operation user.GetUsers has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
