# About #

Simple crud rest api written in golang. 

Used architectural patterns:

```
Mediator
Entity
DTO 
Repository
```

Used libs worth mentioning:

```
swagger-go
gorm
```

# Instalation #

To run service run in cli:

```
docker-compose -f docker-compose.local.yml up --build
```

# Optional Setup #

.env file:

```
DSN=root:password@tcp(172.28.1.4:3306)/superdb?charset=utf8mb4&parseTime=True&loc=Local
```

# Endpoints #

Add user
```
POST
http://[::]:3000/v1/user
```
Edit user
```
PUT
http://[::]:3000/v1/user
```
Delete user
```
DELETE
http://[::]:3000/v1/user/{id}
```
Get user
```
GET
http://[::]:3000/v1/user/{id}
```
Get users
```
GET
http://[::]:3000/v1/user
```

# Detailed Swagger Docs #

Open link below:
https://gitlab.com/donrezo/royceapi/-/blob/master/api/swagger/swagger.yml