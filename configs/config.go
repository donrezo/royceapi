package configs

import (
	"github.com/sakirsensoy/genv"
)

type appConfig struct {
	DSN string
}
var  App = &appConfig{
	DSN: genv.Key("DSN").String(),
}