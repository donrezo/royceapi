package entities

import (
	"github.com/go-openapi/strfmt"
	"royceApi/api/models"
)

type User struct {
	ID string   `gorm:"primaryKey"`
	Address string
	Description string
	Dob strfmt.Date
	Name string
	CreatedAt strfmt.DateTime
	UpdatedAt strfmt.DateTime
}

func(u *User) ConvertToModel() *models.User {
	return &models.User{
		ID: u.ID,
		Address: u.Address,
		Description: u.Description,
		Dob: u.Dob,
		Name: u.Name,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}
}

