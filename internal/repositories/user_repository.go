package repositories

import (
	"royceApi/api/models"
	"gorm.io/gorm"
	"royceApi/api/restapi/operations/user"
	"royceApi/internal/entities"
	"time"
	"github.com/go-openapi/strfmt"
	"github.com/google/uuid"
)

type UserHandler interface {
	FetchUserById(db *gorm.DB, id string) (*models.User, error)
	CreateNewUser(db *gorm.DB, params user.AddUserParams) (*models.User, error)
	DeleteUserById(db *gorm.DB, id string) (*models.User, error)
	EditUserById(db *gorm.DB, id string, params user.EditUserParams) (*models.User, error)
	FetchAllUsers(db *gorm.DB) ([]*models.User, error)
}

type UserRepository struct {}

func ProvideUserRepository() UserHandler {
	return &UserRepository{}
}

func (u *UserRepository) FetchUserById(db *gorm.DB, id string) (*models.User, error){
	entity := entities.User{ID: id}
	result := db.First(&entity)

	if result.Error != nil {
		return nil, result.Error
	}

	return entity.ConvertToModel(), nil
}

func (u *UserRepository) CreateNewUser(db *gorm.DB, params user.AddUserParams) (*models.User, error){

	entity := entities.User{}
	entity.ID = uuid.New().String()
	entity.Address = params.Address
	entity.Name = params.Name
	entity.UpdatedAt = strfmt.DateTime(time.Now())
	entity.CreatedAt = strfmt.DateTime(time.Now())
	entity.Dob = strfmt.Date(time.Unix(int64(params.Dob),0));
	entity.Description = params.Description

	result := db.Create(&entity)
	if result.Error != nil {
		return nil, result.Error
	}

	return entity.ConvertToModel(), nil
}

func (u *UserRepository) DeleteUserById(db *gorm.DB, id string) (*models.User, error){
	result := db.Delete(&entities.User{}, &entities.User{ID: id})

	if result.Error != nil {
		return nil, result.Error
	}

	return nil, nil
}

func (u *UserRepository) EditUserById(db *gorm.DB, id string, params user.EditUserParams) (*models.User, error){
	entity := entities.User{ID: id}

	result := db.First(&entity)

	if result.Error != nil {
		return nil, result.Error
	}

	if (params.Dob != nil) {
		entity.Dob = strfmt.Date(time.Unix(int64(*params.Dob),0));
	}
	if (params.Address != nil) {
		entity.Address = *params.Address
	}
	if (params.Description != nil) {
		entity.Description = *params.Description
	}

	if (params.Name != nil){
		entity.Name = *params.Name
	}

	entity.UpdatedAt = strfmt.DateTime(time.Now())
	result = db.Save(&entity)

	if result.Error != nil {
		return nil, result.Error
	}

	return entity.ConvertToModel(), nil
}

func (u *UserRepository) FetchAllUsers(db *gorm.DB) ([]*models.User, error){

	entities := make([]entities.User,1)
	result := db.Find(&entities)

	if result.Error != nil {
		return nil, result.Error
	}

	models := make([]*models.User,1)

	for _, entity := range entities {
		models = append(models,entity.ConvertToModel())
	}

	return models, nil
}



